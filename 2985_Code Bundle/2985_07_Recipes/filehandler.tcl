 # Check that a filename was provided
if { $argc > 0 & $argv > 0} {
  # Assign the filename to a variable
  set fname [lindex $argv 0]
  # Open the file for read-only access
  set fp [open $fname r]
  # Read the contents of the file into a variable
  set data [read $fp]
  #Close the input file
  close $fp
  # Split the data and add it to a TCL list
  set input [split $data ","] 
  # Sort the list and load it into another list
  set output [lsort -increasing $input]
  # Open a file to write the data to
  set fp [open output.txt w]
  # Read through the list and write the data
  puts $fp [join $output \n]
  #Close the file
  close $fp
} else {
  puts "No filename provided... Exiting Script"
  exit
}
