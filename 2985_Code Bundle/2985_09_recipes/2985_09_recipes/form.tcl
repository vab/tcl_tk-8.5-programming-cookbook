# Load the TK package
package require Tk

#Procedure to add to the listbox
proc list_add { } {
	set s [ .e get ];
	.l insert end $s;
	.e delete 0 end;
}

# Create image and widgets
image create photo logo -file tcllogo.gif
label .logo -image logo
listbox .l -borderwidth 3 -width 25 -height 10
entry .e -width 25
frame .f -width 25 -borderwidth 2 -relief groove
button .f.update -text "Add" -command list_add
button .f.exit -text "Exit" -command exit

# Pack the widgets
pack .logo -side top -anchor w
pack .l -side top -anchor e
pack .f.update -side left
pack .f.exit -side right
pack .f -side bottom -padx 2 -pady 2 -fill both
pack .e -side bottom 
