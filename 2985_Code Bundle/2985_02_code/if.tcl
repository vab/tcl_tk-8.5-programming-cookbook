# Set the variable x to the argument
set x [lindex $argv 0]
if {$x == 1} {# Test for condition 1
   puts "You entered: $x"
} elseif {$x == 2} {# Test for condition 1
   puts "You entered: $x"
} else {# If neither condition is met perform the default action
   puts "$x is not a valid argument"
}
