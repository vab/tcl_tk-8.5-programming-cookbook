# If no command line arguments are passed perform no actions
if {$argc == 2} {
   set x [lindex $argv 0]
   set y [lindex $argv 1]
   puts "Beginning the while loop"
   while {$x >= 0} {
         puts "$x\t"
         incr x
         if {$x == $y} {
            puts "Upper limit reached"
            break
         }
     }
} else {
   puts "Invalid number of arguments"
}

