# While x is less than 11 print out the value of x
for { set x 1 } {$x<11} {incr x} {
    puts "x = $x"
}