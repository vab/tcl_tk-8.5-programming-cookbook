As TCL is a run-time compiled scripting language, all recipes are provided as text.  To execute the scripts requires that a working implementation of TCL be installed and that any files (args.tcl for example) exist within the operators home directory based on platform.

The file args.tcl contains the scripting required for recipe 2.