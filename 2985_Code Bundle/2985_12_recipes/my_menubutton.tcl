# Load the TK Package
package require Tk

#Define our interface
wm geometry . 320x240
wm title . "Menu Example"

# Create a menubutton to exit our window
menubutton .menu1 -text File -menu .menu1.m -underline 0 -relief raised

# Add a pull down
menu .menu1.m
.menu1.m add command -label Quit -command exit

# Pack the menubutton
pack .menu1 -anchor nw
