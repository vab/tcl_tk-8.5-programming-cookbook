# Load the TK Package
package require Tk

#Define our interface
wm geometry . 320x240
wm title . "Menu Example"

# Create a menu to exit our application
menu .myMenu 
. configure -menu .myMenu

# Add a pull down
set File [menu .myMenu.myfile]
.myMenu add cascade -label File -menu .myMenu.myfile

# Add the Exit entry
$File add command -label Exit -command exit
