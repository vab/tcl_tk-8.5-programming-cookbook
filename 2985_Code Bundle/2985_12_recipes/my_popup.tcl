# Load the TK Package
package require Tk

#Define our interface
wm geometry . 320x240
wm title . "Menu Example"

# Create a menu to exit our window
set File [menu .popup]

# Add the Exit entry
$File add command -label Exit -command exit

# Now we add a label to bind to
label .l -text "Click here to access your menu"
pack .l 

# Now bind to the right mouse click 
bind .l <3> {tk_popup .popup %X %Y}
