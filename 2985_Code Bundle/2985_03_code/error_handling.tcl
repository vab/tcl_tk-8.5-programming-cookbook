#Check that arguments were passed
if { $argc >0 } {
   #Define variables for the filename, program
   set fname [lindex $argv 0]
   set progname [lindex $argv 1]
   #Check that the file exists for reading
   set retval [catch {open $fname r} fid]
   #If the file cannot be read
   if {$retval !=0} {
      puts "Unable to open $fname with $progname"
   } else {
      # Attempt to open the file
      set status 0
      if {[catch {eval exec $progname $fname &} results options]} {
         # Obtain the dictionary values for the error
         set details [dict get $options -errorcode]
         # If the program has launched with t a Child Status 
         # proceed as normal
         if {[lindex $details 0] eq "CHILDSTATUS"} {
         set status [lindex $details 2]
         } else {
         # Generate a catch all error
           error "Unexpected failure"
           exit 1
        }
      }
   }
}



