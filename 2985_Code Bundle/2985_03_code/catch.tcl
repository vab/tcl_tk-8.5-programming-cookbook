# Prompt the user for a number
puts -nonewline "Enter a number: "
# Clear standard out
flush stdout
# Assign the argument to a variable (value)
gets stdin value
# Return a doubled value or error message
if {[catch {set doubled [expr $value * 2]} errmsg]} {
puts "Script Failed - $errmsg"
} else {
puts "$value doubled is: $doubled"
}
puts "Regardless of error the script continues…" 
